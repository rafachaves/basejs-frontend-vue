#!/bin/bash

if [ "$APP_MODE" = "development" ]; then
  npm install
  npm run watch-dev

else
  npm run build
fi
exec "$@"
