const axios = require('axios');

const api = axios.create({ baseURL: document.location.origin });

export default api;