import api from './api';
import storage from './storage';

const auth = {
  user: null,

  loggedIn() {
    return !!storage.get('token');
  },  
  
  logout() {
    storage.set('token', null);
    storage.set('strategy', null);
    
    api.defaults.headers.common['Authorization'] = '';
  },
  
  login(token, strategy) {
    storage.set('token', token);
    storage.set('strategy', strategy);
    
    api.defaults.headers.common['Authorization'] = 'Bearer ' + token;
  },

  async getUserData() {
    if(storage.get('token')){
      const response = await api.get('/users/profile');
      return response.data;
    }
  },

  /**
   * Register the user with email and password
   */
  async localRegister(userData) {
    const response = await api.post('/users', userData);
    return response.data;
  },

  /**
   * Obtain the authorization token with email and password
   */
  async localLogin(userData) {
    const response = await api.post('/auth', {strategy: 'local', ...userData});
    this.login(response.data.token, 'local');
  },
}

export default auth;