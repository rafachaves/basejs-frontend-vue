const storage = {
  prefix: 'auth_',
  timeouts: {},
  
  /**
  * get the value of the given key from sessionStorage.
  * 
  * if the sessionStorage is empty but the localStorage has a value,
  * set the value in sessionStorage before return the value
  */
  get(key) {
    key = 'auth_' + key;
    if(!sessionStorage[key] && localStorage[key]){
      sessionStorage[key] = localStorage[key];
    }
    return sessionStorage[key];
  },
  
  /**
  * saves the value of the given key in storages
  * 
  * first saves in the sessionStorage and if the rememberMe
  * options is enabled saves the value in localStorage too.
  * 
  * if the value is null or undefined delete the values from storages.
  */
  set(key, value) {
    key = 'auth_' + key;
    if(value === null || value === undefined){
      delete sessionStorage[key];
      if(this.rememberMe){
        delete localStorage[key]
      }
    } else {
      sessionStorage[key] = value;
      if(this.rememberMe){
        localStorage[key] = sessionStorage[key];
      }
    }
    return value;
  },

  /**
  * set the localStorage key with the value and clear the forget timeout.
  */
  remember(key, value) {
    key = this.prefix + key;
    clearTimeout(this.timeouts[key]);
    localStorage[key] = value;
  },
  
  /**
  * awaits 10 seconds and then deletes the key from localStorage
  */
  forget(key) {
    key = this.prefix + key;
    this.timeouts[key] = setTimeout(() => delete localStorage[key], 1000);
  },
  
}

export default storage;